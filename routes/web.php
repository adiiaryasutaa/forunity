<?php

use App\Http\Controllers\CKEditorController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\AnswerController;
use App\Http\Controllers\TopicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Route::middleware([
	'auth:sanctum',
	config('jetstream.auth_session'),
	'verified'
])->group(function () {
	Route::controller(HomeController::class)->group(function () {
		Route::get('/home', 'index')->name('home');
	});

	Route::controller(DashboardController::class)->group(function () {
		Route::get('/dashboard', 'index')->name('dashboard');
	});

	Route::controller(NotificationController::class)->group(function () {
		Route::get('/notification', 'index')->name('notification');
	});

	Route::controller(TopicController::class)->group(function () {
		Route::get('/topics/create', 'create')->name('topics.create');
		Route::post('/topics/store', 'store')->name('topics.store');
		Route::get('/topics/{topic:slug}', 'show')->name('topics.show');
		Route::get('/topics/{topic:slug}/edit', 'edit')->name('topics.edit');
		Route::get('/topics/{topic:slug}/setting', 'setting')->name('topics.setting');
		Route::patch('/topics/{topic:slug}', 'update')->name('topics.update');
		Route::delete('/topics/{topic:slug}', 'destroy')->name('topics.delete');
	});

	Route::controller(AnswerController::class)->group(function () {
		Route::post('/topics/{topic:slug}/reply', 'store')->name('topics.reply');
	});

	Route::controller(CKEditorController::class)->group(function () {
		Route::post('ckeditor/upload', 'upload')->name('ckeditor.upload');
	});
});
