@php
	use App\Models\Topic;
	use App\Models\Reply;
	use App\Notifications\TopicRepliedNotification;
@endphp

<div>
	<h2 class="text-2xl font-semibold mb-4">{{ __('Unread') }}</h2>
	@forelse($unreadNotifications as $notification)
		@php
			$topic = Topic::find($notification->data['topic_id']);
			$reply = Reply::find($notification->data['reply_id']);
		@endphp
		<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
			<div class="p-4 flex justify-between items-center">
				<div class="flex items-center">
					@switch($notification->type)
						@case(TopicRepliedNotification::class)
							<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
									 stroke="currentColor" class="w-10 h-10 text-gray-500 mr-4">
								<path stroke-linecap="round" stroke-linejoin="round"
											d="M7.5 8.25h9m-9 3H12m-9.75 1.51c0 1.6 1.123 2.994 2.707 3.227 1.129.166 2.27.293 3.423.379.35.026.67.21.865.501L12 21l2.755-4.133a1.14 1.14 0 01.865-.501 48.172 48.172 0 003.423-.379c1.584-.233 2.707-1.626 2.707-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0012 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018z"/>
							</svg>
							{{ __('Your topic (') }}
							<span class="font-semibold">{{ $topic->title }}</span>
							{{ __(') replied by ') }}
							<div class="inline-flex items-center ml-1.5">
								<img src="{{ $reply->user->profile_photo_url }}" class="rounded-full w-5"
										 alt="{{ $reply->user->name }}">
								<span class="font-semibold ml-1">{{ $reply->user->name }}</span>
							</div>
							@break
					@endswitch
				</div>
				<div class="flex items-center">
					<a href="{{ route('topics.show', ['topic' => $topic]) }}">{{ __('See') }}</a>
					<form class="inline" action="{{ '' }}">

					</form>
				</div>
			</div>
		</div>
	@empty
	@endforelse
</div>
