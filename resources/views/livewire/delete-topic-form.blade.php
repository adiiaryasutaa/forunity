<x-jet-action-section>
	<x-slot name="title">
		{{ __('Delete Topic') }}
	</x-slot>

	<x-slot name="description">
		{{ __('Delete topic permanently.') }}
	</x-slot>

	<x-slot name="content">
		<div class="max-w-xl text-sm text-gray-600">
			{{ __('When the topic deleted, its replies will be deleted permanently.') }}
		</div>

		<div class="mt-5">
			<x-jet-danger-button wire:click="confirmTopicDeletion" wire:loading.attr="disabled">
				{{ __('Delete Topic') }}
			</x-jet-danger-button>
		</div>

		<!-- Delete User Confirmation Modal -->
		<x-jet-dialog-modal wire:model="confirmingTopicDeletion">
			<x-slot name="title">
				{{ __('Delete Topic') }}
			</x-slot>

			<x-slot name="content">
				{{ __('Are you sure you want to delete this topic? Once this topic is deleted, all of its resources and data will be permanently deleted. Please enter your password to confirm you would like to permanently delete this topic.') }}

				<div class="mt-4" x-data="{}" x-on:confirming-delete-topic.window="setTimeout(() => $refs.password.focus(), 250)">
					<x-jet-input type="password" class="mt-1 block w-3/4"
											 placeholder="{{ __('Password') }}"
											 x-ref="password"
											 wire:model.defer="password"
											 wire:keydown.enter="deleteTopic" />

					<x-jet-input-error for="password" class="mt-2" />
				</div>
			</x-slot>

			<x-slot name="footer">
				<x-jet-secondary-button wire:click="$toggle('confirmingTopicDeletion')" wire:loading.attr="disabled">
					{{ __('Cancel') }}
				</x-jet-secondary-button>

				<x-jet-danger-button class="ml-3" wire:click="deleteTopic" wire:loading.attr="disabled">
					{{ __('Delete Topic') }}
				</x-jet-danger-button>
			</x-slot>
		</x-jet-dialog-modal>
	</x-slot>
</x-jet-action-section>
