<x-app-layout>
	<x-slot name="header">
		<h2 class="font-semibold text-xl text-gray-800 leading-tight">
			{{ $topic->title }}
		</h2>
	</x-slot>

	<div class="pt-12 pb-40">
		<div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-8">
			<div class="flex justify-end space-x-4">
				@can('update', $topic)
					<a href="{{ route('topics.edit', ['topic' => $topic]) }}"
						 class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">
						{{ __('Edit') }}
					</a>
					<a href="{{ route('topics.setting', ['topic' => $topic]) }}"
						 class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">
						{{ __('Settings') }}
					</a>
				@endcan
			</div>

			@include('forum.topic-container', ['topic' => $topic])

			@include('forum.answer-form', ['topic' => $topic])

			<div>
				<h2 class="text-xl font-semibold mb-4">{{ __(Str::plural('Answer', $topic->answers->count())) }}
					({{ $topic->answers->count() }})</h2>
				<div class="space-y-4">
					@forelse($topic->answers as $answer)
						@include('forum.answer', ['reply' => $answer])
					@empty
						@include('forum.empty-answer')
					@endforelse
				</div>
			</div>
		</div>
	</div>
</x-app-layout>
