<x-jet-action-section submit="updateTopic">
	<x-slot name="title">
		{{ __('Edit') }}
	</x-slot>

	<x-slot name="description">
		{{ __('Update topic') }}
	</x-slot>

	<x-slot name="content">
		<div class="max-w-xl text-sm text-gray-600">
			{{ __('Edit topic information if there\'s something you want to update.') }}
		</div>

		<div class="mt-5">
			<a href="{{ route('topics.edit', ['topic' => $topic]) }}"
				 class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">
				{{ __('Edit') }}
			</a>
		</div>

	</x-slot>
</x-jet-action-section>
