<x-app-layout>
	<x-slot name="header">
		<h2 class="font-semibold text-xl text-gray-800 leading-tight">
			{{ __('Create Topic') }}
		</h2>
	</x-slot>

	<form method="post" action="{{ route('topics.store') }}" enctype="multipart/form-data">
		@csrf
		<div class="py-12">
			<div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-4">
				<div class="flex justify-end">
					<x-jet-button>
						{{ __('Post') }}
					</x-jet-button>
				</div>
				<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
					<div class="p-8">
						<!-- Title -->
						<div>
							<x-jet-label for="title" value="{{ __('Title') }}"/>
							<x-jet-input class="block mt-1 w-full" type="text" id="title" name="title" :value="old('title')"/>
							<x-jet-input-error for="title"/>
						</div>

						<!-- Body -->
						<div class="mt-4">
							<x-jet-label for="body" value="{{ __('Body') }}"/>
							<x-jet-input-error for="body"/>
							<textarea id="body" class="ckeditor" name="body">{{ old('body') }}</textarea>
						</div>

						<!-- Limit replies -->
						<div class="mt-4">
							<x-jet-label for="limit-replies" value="{{ __('Limit replies') }}"/>
							<x-jet-input class="block mt-1 w-full" type="number" id="limit-replies" name="limit_replies" :value="old('limit_replies')" min="8" max="250" value="250"/>
							<x-jet-input-error for="limit-replies"/>
						</div>

						<!-- Block users -->

					</div>
				</div>
			</div>
		</div>
	</form>
</x-app-layout>
