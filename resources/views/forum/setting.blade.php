<x-app-layout>
	<x-slot name="header">
		<h2 class="font-semibold text-xl text-gray-800 leading-tight">
			{{ __("Settings for {$topic->title}") }}
		</h2>
	</x-slot>

	<div>
		<div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
			@include('forum.edit-form')

			<x-jet-section-border/>

			@livewire('delete-topic-form', ['topic' => $topic])
		</div>
	</div>
</x-app-layout>
