@props(['answer'])

<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
	<div class="p-8">
		<div class="flex items-center space-x-2">
			<div class="flex items-center">
				<span class="font-semibold mr-2">{{ __('Answer from') }}</span>
				<img src="{{ $answer->user->profile_photo_url }}" class="rounded-full w-5">
				<span class="font-semibold ml-1">{{ $answer->user->name }}</span>
			</div>
			@if($answer->user->is($topic->user))
				<div class="px-1.5 py-0.5 rounded bg-indigo-50 font-semibold text-sm">
					{{ __('poster') }}
				</div>
			@endif
			<div class="text-gray-500 before:content-['\00B7']">
				{{ $answer->created_at->diffForHumans() }}
			</div>
		</div>
		<div class="mt-4">
			{!! $answer->body !!}
		</div>
	</div>
</div>
