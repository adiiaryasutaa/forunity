<x-app-layout>
	<x-slot name="header">
		<h2 class="font-semibold text-xl text-gray-800 space-x-2 leading-tight">
			<span>{{ __('Edit Topic ') . $topic->title }}</span>
		</h2>
	</x-slot>

	<form method="post" action="{{ route('topics.update', ['topic' => $topic]) }}" enctype="multipart/form-data">
		@csrf
		@method('PATCH')
		<div class="py-12">
			<div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-4">
				<div class="flex justify-end">
					<x-jet-button>
						{{ __('Save') }}
					</x-jet-button>
				</div>
				<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
					<div class="p-8">
						<div>
							<x-jet-label for="title" value="{{ __('Title') }}"/>
							<x-jet-input class="block mt-1 w-full" type="text" id="title" name="title" :value="$topic->title"/>
							<x-jet-input-error for="title"/>
						</div>
						<div class="mt-4">
							<x-jet-label for="body" value="{{ __('Body') }}"/>
							<x-jet-input-error for="body"/>
							<textarea id="body" class="ckeditor" name="body">{{ $topic->body }}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</x-app-layout>
