@props(['topic'])

<div>
	<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
		<form action="{{ route('topics.reply', ['topic' => $topic]) }}" method="post" enctype="multipart/form-data">
			@csrf
			<div class="p-8">
				<div class="flex items-center">
					<span class="font-semibold mr-2">{{ __('Answering to') }}</span>
					<img src="{{ $topic->user->profile_photo_url }}" class="rounded-full w-5">
					<span class="font-semibold ml-1">{{ $topic->user->name }}</span>
				</div>
				<div class="mt-4">
					<x-jet-input-error for="body"/>
					<textarea id="body" class="ckeditor" name="body"></textarea>
				</div>
				<div class="flex justify-end mt-4">
					<x-jet-button>
						{{ __('Post') }}
					</x-jet-button>
				</div>
			</div>
		</form>
	</div>
</div>
