<x-app-layout>
	<x-slot name="header">
		<h2 class="font-semibold text-xl text-gray-800 leading-tight">
			{{ __('Home') }}
		</h2>
	</x-slot>

	<div class="py-12">
		<div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-4">
			<div class="flex justify-end">
				<a href="{{ route('topics.create') }}"
					 class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">
					{{ __('Create') }}
				</a>
			</div>
			<div class="space-y-4">
				@forelse($topics as $topic)
					@include('forum.index-topic', ['topic' => $topic])
				@empty
					@include('forum.empty-forum')
				@endforelse
			</div>
		</div>
	</div>
</x-app-layout>
