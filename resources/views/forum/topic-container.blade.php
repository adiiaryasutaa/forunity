@props(['topic'])

<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
	<div class="p-8">
		<div class="flex justify-between items-center">
			<div class="flex items-center space-x-2">
				<div class="flex items-center">
					<img src="{{ $topic->user->profile_photo_url }}" class="rounded-full w-8">
					<span class="font-semibold ml-2">{{ $topic->user->name }}</span>
				</div>
				<div class="text-gray-500 before:content-['\00B7']">
					{{ $topic->created_at->diffForHumans() }}
				</div>
				@if($topic->created_at->notEqualTo($topic->updated_at))
					<div class="text-gray-500">
						{{ __('(edited)') }}
					</div>
				@endif
			</div>
		</div>
		<h1 class="font-semibold text-2xl text-gray-800 leading-tight mt-8">
			{{ $topic->title }}
		</h1>
		<div class="mt-4 space-y-4">
			{!! $topic->body !!}
		</div>
	</div>
</div>
