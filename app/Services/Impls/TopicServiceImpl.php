<?php

namespace App\Services\Impls;

use App\Models\Topic;
use App\Services\TopicService;

class TopicServiceImpl implements TopicService
{
	public function create(array $data): ?Topic
	{
		$user = auth()->user();

		return $user->topics()->create([
			'title' => str($data['title'])->title()->toString(),

			'slug' => str(uuid_create() . " " . $data['title'])->slug(),

			'excerpt' => str(trim(preg_replace('/\s\s+/', ' ', strip_tags($data['body']))))
				->excerpt(options: ['radius' => 1020]),

			'body' => $data['body'],

			'max_replies' => $data['max_replies'],
		]);
	}

	public function update(Topic $topic, array $data): ?Topic
	{
		// TODO: Implement update() method.
	}

	public function delete(Topic $topic): bool
	{
		// TODO: Implement delete() method.
	}
}
