<?php

namespace App\Services;

use App\Models\Topic;

interface TopicService
{
	public function create(array $data): ?Topic;

	public function update(Topic $topic, array $data): ?Topic;

	public function delete(Topic $topic): bool;
}
