<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends Model
{
	use HasFactory;

	protected $fillable = [
		'name'
	];

	public function topics(): BelongsToMany
	{
		return $this->belongsToMany(Topic::class)->using(CategoryTopic::class);
	}
}
