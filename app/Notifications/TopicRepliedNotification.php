<?php

namespace App\Notifications;

use App\Models\Answer;
use App\Models\Topic;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TopicRepliedNotification extends Notification
{
	use Queueable;

	public Topic $topic;
	public Answer $answer;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Topic $topic, Answer $answer)
	{
		$this->topic = $topic;
		$this->answer = $answer;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param mixed $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return ['database'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param mixed $notifiable
	 * @return MailMessage
	 */
	public function toMail($notifiable)
	{
		return (new MailMessage)
			->line('The introduction to the notification.')
			->action('Notification Action', url('/'))
			->line('Thank you for using our application!');
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param mixed $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}

	public function toDatabase($notifiable)
	{
		return [
			'topic' => $this->topic,
			'answer' => $this->answer,
		];
	}
}
