<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CKEditorController extends Controller
{
	public function upload(Request $request)
	{
		if ($request->hasFile('upload')) {
			$file = $request->file('upload');

			$originName = $file->getClientOriginalName();
			$fileName = pathinfo($originName, PATHINFO_FILENAME);
			$extension = $file->getClientOriginalExtension();

			$fileName = $fileName . '_' . time() . '.' . $extension;
			$file->move(public_path('images'), $fileName);

			$CKEditorFuncNum = $request->input('CKEditorFuncNum');
			$url = asset("images/$fileName");
			$message = 'Image uploaded successfully';
			$response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$message')</script>";

			@header('Content-type: text/html; charset=utf-8');
			echo $response;
		}
	}
}
