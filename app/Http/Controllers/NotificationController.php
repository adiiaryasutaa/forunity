<?php

namespace App\Http\Controllers;

use App\Notifications\TopicRepliedNotification;

class NotificationController extends Controller
{
	public function index()
	{
		$user = auth()->user();

		// Notification N + 1 Problem

		return view('notification', [
			'unreadNotifications' => $user->unreadNotifications,
			'readNotifications' => $user->readNotifications,
		]);
	}

	public function markNotificationAsRead(TopicRepliedNotification $notification)
	{

	}
}
