<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTopicRequest;
use App\Http\Requests\UpdateTopicRequest;
use App\Models\Topic;
use App\Services\TopicService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Arr;

class TopicController extends Controller
{
	private TopicService $topicService;

	/**
	 * @param TopicService $topicService
	 */
	public function __construct(TopicService $topicService)
	{
		$this->topicService = $topicService;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Application|Factory|View
	 */
	public function create()
	{
		return view('forum.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateTopicRequest $request
	 * @return Application|RedirectResponse
	 */
	public function store(CreateTopicRequest $request)
	{
		$validated = $request->validated();

		$validated['max_replies'] = $validated['limit_replies'];

		$topic = $this->topicService
			->create(Arr::only($validated, ['title', 'body', 'max_replies']));

		if ($topic) {
			return redirect(route('topics.show', ['topic' => $topic]));
		}

		return back()
			->withErrors(['create-topic-failed' => 'Create topic failed']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param Topic $topic
	 * @return Application|Factory|View
	 */
	public function show(Topic $topic)
	{
		$topic = $topic->with(['user', 'answers'])->find($topic->id);

		return view('forum.show', ['topic' => $topic]);
	}

	/**
	 * @throws AuthorizationException
	 */
	public function setting(Topic $topic)
	{
		$this->authorize('update', $topic);

		return view('forum.setting', ['topic' => $topic]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Topic $topic
	 * @return Application|Factory|View
	 * @throws AuthorizationException
	 */
	public function edit(Topic $topic)
	{
		$this->authorize('update', $topic);

		return view('forum.edit', ['topic' => $topic]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param UpdateTopicRequest $request
	 * @param Topic $topic
	 * @return Application|RedirectResponse|Redirector
	 * @throws AuthorizationException
	 */
	public function update(UpdateTopicRequest $request, Topic $topic)
	{
		$this->authorize('update', $topic);

		$data = $request->validated();

		if ($topic->update($data)) {
			$topic = Topic::find($topic->id);

			return redirect(route('topics.show', ['topic' => $topic]))
				->with(['update-topic-success' => __('Topic updated successfully')]);
		}

		return back()
			->withErrors(['update-topic-failed' => __('Topic failed to update')]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Topic $topic
	 * @return Application|Redirector|RedirectResponse
	 * @throws AuthorizationException
	 */
	public function destroy(Topic $topic)
	{
		$this->authorize('delete', $topic);

		if ($topic->delete()) {
			return redirect(route('home'))
				->with(['delete-topic-success' => __('Topic deleted successfully')]);
		}

		return back()
			->withErrors(['delete-topic-failed' => __('Topic failed to delete')]);
	}
}
