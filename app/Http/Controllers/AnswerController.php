<?php

namespace App\Http\Controllers;

use App\Events\TopicAnswered;
use App\Exceptions\FailedCreateAnswerException;
use App\Http\Requests\CreateAnswerRequest;
use App\Models\Topic;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Throwable;

class AnswerController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateAnswerRequest $request
	 * @param Topic $topic
	 * @return Application|ResponseFactory|RedirectResponse|Response
	 * @throws Throwable
	 */
	public function store(CreateAnswerRequest $request, Topic $topic)
	{
		$validated = $request->validated();

		try {
			DB::beginTransaction();

			$answer = $topic->answers()->create(array(...$validated, 'user_id' => $request->user()->id));
			throw_unless($answer, new FailedCreateAnswerException($topic));

			TopicAnswered::dispatchIf(
				$topic->user->isNot($request->user()),
				$topic, $answer
			);

			DB::commit();

			return back();
		} catch (Throwable $e) {
			DB::rollBack();
			dd($e);
			return back()->with(['create-answer-failed' => 'Failed to post your answer']);
		}
	}
}
