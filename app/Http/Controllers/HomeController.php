<?php

namespace App\Http\Controllers;

use App\Models\Topic;

class HomeController extends Controller
{
	public function index()
	{
		$topics = Topic::with('user', 'answers')
			->orderByDesc('created_at')
			->get();

		return view('forum.index', ['topics' => $topics]);
	}
}
