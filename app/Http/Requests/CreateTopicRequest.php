<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTopicRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array<string, mixed>
	 */
	public function rules()
	{
		return [
			'title' => ['bail', 'required', 'string', 'min:10', 'max:255'],
			'body' => ['bail', 'required', 'string', 'min:10'],
			'limit_replies' => ['bail', 'required', 'numeric', 'min:8', 'max:250'],
		];
	}
}
