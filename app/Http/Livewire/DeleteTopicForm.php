<?php

namespace App\Http\Livewire;

use App\Models\Topic;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

class DeleteTopicForm extends Component
{
	/**
	 * Indicates if topic deletion is being confirmed.
	 *
	 * @var bool
	 */
	public $confirmingTopicDeletion = false;

	/**
	 * The user's current password.
	 *
	 * @var string
	 */
	public $password = '';

	/**
	 * The topic want to delete
	 *
	 * @var Topic
	 */
	public $topic;

	public function confirmTopicDeletion()
	{
		$this->resetErrorBag();

		$this->password = '';

		$this->dispatchBrowserEvent('confirming-delete-topic');

		$this->confirmingTopicDeletion = true;
	}

	/**
	 */
	public function deleteTopic(): Redirector|RedirectResponse|Application
	{
		$this->resetErrorBag();

		if (!Hash::check($this->password, Auth::user()->password)) {
			throw ValidationException::withMessages([
				'password' => [__('This password does not match our records.')],
			]);
		}

		if ($this->topic->delete()) {
			return redirect(route('home'))
				->with(['delete-topic-success' => __('Topic deleted successfully')]);
		}

		return back()
			->withErrors(['delete-topic-failed' => __('Topic failed to delete')]);
	}

	public function render()
	{
		return view('livewire.delete-topic-form');
	}
}
