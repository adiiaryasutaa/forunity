<?php

namespace App\Http\Livewire\Notification;

use Livewire\Component;

class NotificationList extends Component
{
	public $notifications;

	public function mount()
	{
//		$this->notifications = User::with('notifications')->find(auth()->user()->id)->get()->dd();
		$this->notifications = auth()->user()->notifications->dd();

		dd($this->notifications);
	}

	public function render()
	{
		return view('livewire.notification.notification-list');
	}

	public function deleteNotification(int $id)
	{
		auth()->user()->notifications->find($id)->delete();
	}
}
