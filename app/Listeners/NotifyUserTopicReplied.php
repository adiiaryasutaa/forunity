<?php

namespace App\Listeners;

use App\Events\TopicAnswered;
use App\Notifications\TopicRepliedNotification;

class NotifyUserTopicReplied
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param TopicAnswered $event
	 * @return void
	 */
	public function handle(TopicAnswered $event)
	{
		$event->topic->user->notify(
			new TopicRepliedNotification($event->topic, $event->answer)
		);
	}
}
