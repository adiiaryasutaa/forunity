<?php

namespace App\Events;

use App\Models\Answer;
use App\Models\Reply;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TopicAnswered
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public Topic $topic;
	public Answer $answer;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(Topic $topic, Answer $answer)
	{
		$this->topic = $topic;
		$this->answer = $answer;
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return \Illuminate\Broadcasting\Channel|array
	 */
	public function broadcastOn()
	{
		return new PrivateChannel('channel-name');
	}
}
