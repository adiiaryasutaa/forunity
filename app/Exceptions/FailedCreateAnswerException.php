<?php

namespace App\Exceptions;

use App\Models\Topic;
use Exception;

class FailedCreateAnswerException extends Exception
{
	/**
	 * @inheritDoc
	 */
	public function __construct(Topic $topic)
	{
		parent::__construct("Failed to create reply for topic id {$topic->id}");
	}
}
