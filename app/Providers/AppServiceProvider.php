<?php

namespace App\Providers;

use App\Services\Impls\TopicServiceImpl;
use App\Services\TopicService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	public array $singletons = [
		TopicService::class => TopicServiceImpl::class,
	];

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}
}
